import React, {createContext, useContext, useState} from 'react';

const CounterContext = createContext<any>(null);

interface ICounterProvider {
  cnt: number;
  children: JSX.Element | JSX.Element[];
}

export const CounterProvider = ({cnt, children}: ICounterProvider) => {
  const [counter, setCounter] = useState<number>(cnt);

  return <CounterContext.Provider value={{counter, setCounter}}>{children}</CounterContext.Provider>;
};

export const useCounter = () => useContext(CounterContext);

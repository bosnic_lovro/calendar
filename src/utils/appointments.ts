import {getDay, getDate} from 'date-fns';
import {Days, evenDayHours, oddDayHours} from '../lib/constants';
import {Day, WorkingHour} from '../lib/constants/types';

export const oneOrZero = () => (Math.random() >= 0.5 ? 1 : 0);

export const shiftHours = (day: Date): number[] => (getDate(day) % 2 ? oddDayHours : evenDayHours);

export const isSun = (day: Date): boolean => Days[getDay(day)] === 'Sun';

export const isOddSat = (day: Date): boolean => Days[getDay(day)] === 'Sat' && !(getDate(day) % 2 === 0);

export const getAllAppointments = (weekDays: Date[]): Day[] =>
  weekDays
    .filter((day: Date) => !(isSun(day) || isOddSat(day)))
    .map((day: Date) => {
      const shiftHours: number[] = getDate(day) % 2 ? oddDayHours : evenDayHours;

      const isBreak = (hour: number) => {
        if (getDate(day) % 2) return hour === 16;
        else return hour === 11;
      };

      return {
        date: day,
        workingHours: shiftHours.map((hour: number) => ({
          start: hour,
          break: isBreak(hour),
          appointments: {
            first: {
              available: !isBreak(hour), // is daily break
              selected: false,
            },
            second: {
              available: true,
              selected: false,
            },
          },
        })),
      };
    });

export const randomAppointmentsByDay = (parts: number) => {
  const numberOfAppointments: number = 15;
  const min: number = 1;
  const randombit: number = numberOfAppointments - min * parts;
  const out: number[] = [];

  for (let i = 0; i < parts; i++) {
    out.push(Math.random());
  }

  const mult = randombit / out.reduce((a, b) => a + b, 0);

  const rands = out.map((el) => Math.round(el * mult + min));

  const sum = rands.reduce((a, b) => a + b, 0);

  if (sum > numberOfAppointments) {
    rands[Math.round(Math.random() * 6)] -= 1;
  } else if (sum < numberOfAppointments) {
    rands[Math.round(Math.random() * 6)] += 1;
  }
  return rands;
};

export const setRandomAppointments = (weekDays: Date[]) => {
  const workingWeekDays: Date[] = Object.values(weekDays).filter((day: Date) => !(isSun(day) || isOddSat(day)));
  const numberOfWorkingDays: number = workingWeekDays.length;
  const randomAppointments: number[] = randomAppointmentsByDay(numberOfWorkingDays);

  const allAppointments: Day[] = getAllAppointments(workingWeekDays);

  const populatedAppointments = allAppointments.map((day: Day, index: number) => {
    const dailyAppointments: number = randomAppointments[index];

    const workingHours: number[] = day.workingHours.map(({start}: WorkingHour) => start);

    const randomHours: number[] = getRandomHours(workingHours, dailyAppointments);

    return {
      date: day.date,
      workingHours: day.workingHours.map((workingHour: WorkingHour) => {
        if (randomHours.includes(workingHour.start) && workingHour.break) {
          return {
            start: workingHour.start,
            break: workingHour.break,
            appointments: {
              first: workingHour.appointments.first,
              second: {
                selected: workingHour.appointments.second.selected,
                available: false,
              },
            },
          };
        } else if (randomHours.includes(workingHour.start) && !workingHour.break) {
          const randomAppointment = oneOrZero() ? 'first' : 'second';
          return {
            start: workingHour.start,
            break: workingHour.break,
            appointments: {
              ...workingHour.appointments,
              [randomAppointment]: {
                selected: workingHour.appointments[randomAppointment].selected,
                available: false,
              },
            },
          };
        } else return workingHour;
      }),
    };
  });

  return populatedAppointments;
};

export const getRandomHours = (workingHours: number[], size: number) => {
  const result: any[] = new Array(size);
  let len: number = workingHours.length;
  const taken: any[] = new Array(len);

  while (size--) {
    var x = Math.floor(Math.random() * len);
    result[size] = workingHours[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
};

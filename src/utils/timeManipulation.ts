export const isoTransform = (date: string | number | Date) => {
    const newDate = new Date(date);
    let day: number | string = newDate.getDate();
    let month: number | string = newDate.getMonth() + 1;
    const year = newDate.getUTCFullYear();
    if (day < 10) day = '0' + day;
    if (month < 10) month = '0' + month;
    return `${year}-${month}-${day}`;
  };
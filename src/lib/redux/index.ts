import {combineReducers} from 'redux';
import {dateReducer} from './reducers/dateReducer';
import {appointmentsReducer} from './reducers/appointmentsReducer';
import {toastReducer} from './reducers/toastReducer';

const rootReducer = combineReducers({
  appointments: appointmentsReducer,
  date: dateReducer,
  toastData: toastReducer,
});

export default rootReducer;

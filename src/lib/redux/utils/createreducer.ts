// @ts-ignore
export const createReducer = (initialState, actionHandlers) => (state = initialState, action) => {
  const reduceFn = actionHandlers[action.type];
  if (reduceFn) {
    return reduceFn(state, action);
  } else {
    return state;
  }
};

import {applyMiddleware, createStore} from 'redux';
import rootReducer from './index';
import {composeWithDevTools} from 'redux-devtools-extension';

const bindMiddleware = (middleware: any) => {
  if (process.env.NODE_ENV !== 'production') {
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

export const makeStore = () => {
  return createStore(rootReducer, bindMiddleware([]));
};

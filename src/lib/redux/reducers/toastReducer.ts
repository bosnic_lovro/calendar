import {createReducer} from '../utils/createreducer';
import {TOAST} from '../constants';
import {ToastContent, ToastOptions} from 'react-toastify';

const SET_TOAST = (
  _state: any,
  {payload: {content, options}}: {payload: {content: ToastContent; options?: ToastOptions}}
) => {
  return {
    content,
    options: options ? options : null,
  };
};

const handlers = {
  [TOAST.SET_TOAST]: SET_TOAST,
};

export const toastReducer = createReducer({}, handlers);

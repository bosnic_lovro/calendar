import {createReducer} from '../utils/createreducer';
import {APPOINTMENTS} from '../constants';
import {setRandomAppointments} from '../../../utils/appointments';
import {Day, WorkingHour} from '../../constants/types';
import {isSameDay} from 'date-fns';

const SET_AVAILABLE_APPOINTMENTS = (_state: any, {payload: weekDays}: {payload: Date[]}) => {
  const appointments = setRandomAppointments(weekDays);
  return [...appointments];
};

const SET_SELECTED_APPOINTMENT = (
  appointments: Day[],
  {payload: appointment}: {payload: {thisHour: WorkingHour; date: Date; isAdd: boolean}}
) => {
  const updatedAppointments = appointments.map((day: Day) => {
    if (!isSameDay(appointment.date, day.date)) return day;
    else
      return {
        date: day.date,
        workingHours: day.workingHours.map((workingHour: WorkingHour) => {
          if (workingHour.start !== appointment.thisHour.start) return workingHour;
          else
            return {
              ...appointment.thisHour,
            };
        }),
      };
  });

  return [...updatedAppointments];
};

const handlers = {
  [APPOINTMENTS.SET_AVAILABLE_APPOINTMENTS]: SET_AVAILABLE_APPOINTMENTS,
  [APPOINTMENTS.SET_SELECTED_APPOINTMENT]: SET_SELECTED_APPOINTMENT,
};

export const appointmentsReducer = createReducer([], handlers);

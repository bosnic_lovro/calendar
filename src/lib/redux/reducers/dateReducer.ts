import {APP_DATE} from '../constants';
import {createReducer} from '../utils/createreducer';
import {isoTransform} from '../../../utils/timeManipulation';

const SET_DATE = (_state: Date, {payload}: {payload: Date}) => {
  const newDate = isoTransform(new Date(payload));
  return newDate;
};

const handlers = {
  [APP_DATE.SET_DATE]: SET_DATE,
};

export const dateReducer = createReducer(isoTransform(new Date()), handlers);

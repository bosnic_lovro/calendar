import {APP_DATE} from '../constants';

export const setDate = (date: Date) => ({
  type: APP_DATE.SET_DATE,
  payload: date,
});

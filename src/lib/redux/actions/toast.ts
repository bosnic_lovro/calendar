import {ToastContent, ToastOptions} from 'react-toastify';
import {TOAST} from '../constants';

export const setToast = (content: ToastContent, options?: ToastOptions) => ({
  type: TOAST.SET_TOAST,
  payload: {content, options},
});

import {APPOINTMENTS} from '../constants';
import {WorkingHour} from '../../constants/types';

export const setAvailableAppointments = (appointments: Date[]) => ({
  type: APPOINTMENTS.SET_AVAILABLE_APPOINTMENTS,
  payload: appointments,
});

export const setSelectedAppointment = (appointment: {thisHour: WorkingHour; date: Date; isAdd: boolean}) => ({
  type: APPOINTMENTS.SET_SELECTED_APPOINTMENT,
  payload: appointment,
});

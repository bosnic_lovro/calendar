import { MonthShort } from "./types";

export const Months: string[] = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const MonthsShort: MonthShort = {
  0: 'Jan',
  1: 'Feb',
  2: 'Mar',
  3: 'Apr',
  4: 'May',
  5: 'Jun',
  6: 'Jul',
  7: 'Aug',
  8: 'Sep',
  9: 'Oct',
  10: 'Nov',
  11: 'Dec',
};

export const Days: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export const allDayHours: number[] = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];

export const evenDayHours: number[] = [8, 9, 10, 11, 12, 13];

export const oddDayHours: number[] = [13, 14, 15, 16, 17, 18];

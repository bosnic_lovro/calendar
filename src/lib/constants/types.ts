export type ShownPeriodType = {
  from: {
    hour: number;
    minute: number;
  };
  isChecked: boolean;
  label: string;
  to: {
    hour: number;
    minute: number;
  };
};

export type ShiftHours = {
  start: number;
  end: number;
};

export type Appointment = {
  available: boolean;
  selected: boolean;
};

export type HourAppointments = {
  first: Appointment;
  second: Appointment;
};

export type WorkingHour = {
  start: number;
  break: boolean;
  appointments: HourAppointments;
};

export type Day = {
  date: Date;
  workingHours: WorkingHour[];
};

export type MonthShort = {
  [key: number]: string;
};

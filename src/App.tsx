import React from 'react';
import Week from './components/week';
import {Provider} from 'react-redux';
import {makeStore} from './lib/redux/redux';

const App = () => {
  const store = makeStore();

  return (
    <Provider store={store}>
      <Week />
    </Provider>
  );
};

export default App;

import {isSameDay} from 'date-fns';
import React, {FunctionComponent} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useCounter} from '../hooks/counter';
import {Day, WorkingHour} from '../lib/constants/types';
import {setSelectedAppointment} from '../lib/redux/actions/appointments';
import {setToast} from '../lib/redux/actions/toast';
import styles from './style.module.css';

interface IHour {
  day: Date;
  hour: number;
  isSun: boolean;
}

const Hour: FunctionComponent<IHour> = ({day, hour, isSun}): JSX.Element => {
  const dispatch = useDispatch();
  const {counter, setCounter} = useCounter();
  const {appointments} = useSelector((state: any) => state);
  const appointment: Day | null = appointments.find(({date}: Day) => isSameDay(day, date)) || null;
  if (
    isSun ||
    !appointment ||
    !appointment.workingHours.find((workingHour: WorkingHour) => workingHour.start === hour)
  ) {
    return (
      <>
        <div className={`h-50 border-bottom ${styles.notWorking}`} />
        <div className={`h-50 ${styles.notWorking}`} />
      </>
    );
  }

  const {date, workingHours} = appointment;

  const thisHour: WorkingHour = workingHours.find((workingHour: WorkingHour) => workingHour.start === hour)!;

  const backgroundColor = (firstOrSecond: string): string => {
    if (firstOrSecond === 'first' && thisHour.break) return styles.disabled;
    else if (!thisHour.appointments[firstOrSecond].available) return styles.takenBg;
    else if (thisHour.appointments[firstOrSecond].selected) return styles.selected;
    else if (thisHour.appointments[firstOrSecond].available) return styles.availableBg;
    else return styles.disabled;
  };

  const time = (firstOrSecond: string): string => {
    if (hour.toString().length < 2) return `0${hour}:${firstOrSecond === 'first' ? '00' : '30'}`;
    else return `${hour}:${firstOrSecond === 'first' ? '00' : '30'}`;
  };

  const removeAppointment = (firstOrSecond: 'first' | 'second') => {
    if (thisHour.appointments[firstOrSecond].selected) {
      thisHour.appointments[firstOrSecond].selected = false;
      dispatch(setSelectedAppointment({date, thisHour, isAdd: false}));
      dispatch(setToast('Appointment successfully removed.', {type: 'success'}));
      setCounter((prevState) => prevState - 1);
    }
  };

  const selectAppointment = (firstOrSecond: 'first' | 'second') => {
    const dayAlreadySelected: boolean = workingHours.some((workingHour: WorkingHour) =>
      workingHour.appointments.first.selected || workingHour.appointments.second.selected ? true : false
    );

    if (counter === 2) {
      if (!thisHour.appointments[firstOrSecond].selected) {
        return dispatch(setToast('You can have only two appointments per week.', {type: 'warning'}));
      }
      return removeAppointment(firstOrSecond);
    }

    if (counter < 2) {
      if (dayAlreadySelected && thisHour.appointments[firstOrSecond].selected) {
        removeAppointment(firstOrSecond);
      } else if (dayAlreadySelected) {
        dispatch(setToast('You can book only one appointment per day.', {type: 'warning'}));
      } else {
        thisHour.appointments[firstOrSecond].selected = true;
        dispatch(setSelectedAppointment({date, thisHour, isAdd: true}));
        dispatch(setToast('Appointment successfully added.', {type: 'success'}));
        setCounter((prevState: number) => prevState + 1);
      }
    }
  };

  return (
    <>
      <div
        onClick={() =>
          thisHour.appointments.first.available || thisHour.appointments.first.selected
            ? thisHour.appointments.first.selected
              ? removeAppointment('first')
              : selectAppointment('first')
            : {}
        }
        className={`h-50 d-flex border-bottom`}>
        <div className={`${backgroundColor('first')} ${styles.hourWrap} ${styles.darkText} ${styles.largerText}`}>
          <span>{time('first')}</span>
        </div>
      </div>
      <div
        onClick={() =>
          thisHour.appointments.second.available || thisHour.appointments.second.selected
            ? thisHour.appointments.second.selected
              ? removeAppointment('second')
              : selectAppointment('second')
            : {}
        }
        className={`h-50 d-flex`}>
        <div className={`${backgroundColor('second')} ${styles.hourWrap} ${styles.darkText} ${styles.largerText}`}>
          <span>{time('second')}</span>
        </div>
      </div>
    </>
  );
};

export default Hour;

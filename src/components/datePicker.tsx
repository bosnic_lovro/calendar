import React, {FunctionComponent} from 'react';
import {isBefore, subWeeks, subDays, addWeeks, getDate, getMonth, addDays, getYear} from 'date-fns';
import {useDispatch} from 'react-redux';
import {ChevronLeft, ChevronRight} from '../icons';
import {setDate} from '../lib/redux/actions/date';
import styles from './style.module.css';
import {useCounter} from '../hooks/counter';

interface IDatePicker {
  date: Date;
}

const DatePicker: FunctionComponent<IDatePicker> = ({date}): JSX.Element => {
  const dispatch = useDispatch();

  const {setCounter} = useCounter();

  const calendarDateStart = getDate(new Date(date)) + 1;
  const calendarMonthStart = getMonth(new Date(date)) + 1;
  const calendarDateEnd = getDate(addDays(new Date(date), 7));
  const calendarMonthEnd = getMonth(addDays(new Date(date), 7)) + 1;

  const weekBefore = () => {
    if (!isBefore(subWeeks(new Date(date), 1), subDays(new Date(), 1))) {
      setCounter(0);
      dispatch(setDate(subWeeks(new Date(date), 1)));
    }
  };
  const weekAfter = () => {
    setCounter(0);
    dispatch(setDate(addWeeks(new Date(date), 1)));
  };

  return (
    <div className={styles.datePickerRoot}>
      <div className={styles.datePickerContent}>
        <div className={styles.datePickerContainer}>
          <div onClick={weekBefore}>
            <ChevronLeft dimensions={'20px'} />
          </div>
          <div className={styles.headerDate}>
            {calendarDateStart +
              '.' +
              calendarMonthStart +
              '. - ' +
              calendarDateEnd +
              '.' +
              calendarMonthEnd +
              '.   ' +
              getYear(new Date(date)) +
              '.'}
          </div>
          <div onClick={weekAfter}>
            <ChevronRight dimensions={'20px'} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default DatePicker;

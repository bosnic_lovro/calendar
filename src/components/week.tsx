import React, {FunctionComponent, useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {addDays, isSameDay} from 'date-fns';
import Day from './day';
import {setAvailableAppointments} from '../lib/redux/actions/appointments';
import styles from './style.module.css';
import DatePicker from './datePicker';
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer, toast} from 'react-toastify';
import {setToast} from '../lib/redux/actions/toast';
import {CounterProvider} from '../hooks/counter';

const Week: FunctionComponent = (): JSX.Element => {
  const {date, toastData} = useSelector((state: any) => state);
  const dispatch = useDispatch();

  const [weekDays, setWeekDays] = useState<Date[]>(
    Array.from({length: 7}).map((_, index) => addDays(new Date(date), index + 1))
  );

  useEffect(() => {
    if (!isSameDay(weekDays[0], date)) {
      setWeekDays(Array.from({length: 7}).map((_, index) => addDays(new Date(date), index + 1)));
    }
  }, [date]);

  useEffect(() => {
    dispatch(setAvailableAppointments(weekDays));
  }, [weekDays]);

  useEffect(() => {
    const toastOptios = {
      toastId: 'calendarToast',
      position: toast.POSITION.TOP_CENTER,
      autoClose: 5000,
    };

    if (toastData?.options?.type === 'warning') {
      toast.warning(toastData.content, toastOptios);
    } else if (toastData?.options?.type === 'info') {
      toast.info(toastData.content, toastOptios);
    } else {
      toast.success(toastData.content, toastOptios);
    }
    toast.clearWaitingQueue({containerId: 'calendarToast'});
  }, [toastData]);

  useEffect(() => {
    dispatch(setToast('Select an empty field to add an appointment.', {type: 'info'}));
  }, []);

  return (
    <CounterProvider cnt={0}>
      <ToastContainer
        containerId="calendarToast"
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <DatePicker date={date} />
      <div className={`w-100 d-flex flex-row ${styles.week}`}>
        {weekDays.map((day: Date, index: number) => Day({day, key: index}))}
      </div>
    </CounterProvider>
  );
};

export default Week;

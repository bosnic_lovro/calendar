import React, {FunctionComponent} from 'react';
import {getDay} from 'date-fns';
import {Days} from '../lib/constants';
import Hours from './hours';
import styles from './style.module.css';

interface IDay {
  day: Date;
  key: number;
}

const Day: FunctionComponent<IDay> = ({day, key}): JSX.Element => (
  <div key={key} className={`w-100 d-flex justify-content-center flex-wrap flex-column text-center ${styles.day}`}>
    <div className={`${styles.headerHour}`}>
      <span className={styles.defaultCursor}>{Days[getDay(day)]}</span>
    </div>
    <Hours day={day} />
  </div>
);

export default Day;

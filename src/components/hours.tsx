import React, {FunctionComponent} from 'react';
import {allDayHours} from '../lib/constants';
import Hour from './hour';
import {isSun} from '../utils/appointments';
import styles from './style.module.css';

interface IHour {
  day: Date;
}

const Hours: FunctionComponent<IHour> = ({day}): JSX.Element => {
  return (
    <>
      {allDayHours.map((hour: number) => {
        return (
          <div key={hour} className={styles.hour}>
            <Hour day={day} hour={hour} isSun={isSun(day)} />
          </div>
        );
      })}
    </>
  );
};

export default Hours;

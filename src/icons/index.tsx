export const ChevronRight = ({dimensions}: {dimensions: string}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={dimensions}
      height={dimensions}
      className="bi bi-chevron-right"
      viewBox="0 0 16 16">
      <path
        fillRule="evenodd"
        d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
      />
    </svg>
  );
};

export const ChevronLeft = ({dimensions}: {dimensions: string}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={dimensions}
      height={dimensions}
      className="bi bi-chevron-left"
      viewBox="0 0 16 16">
      <path
        fillRule="evenodd"
        d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
      />
    </svg>
  );
};
